//
// Created by root on 06.03.2021.
//

#ifndef LIBRARY_ARCHIVE_EXT_AREARCHIVE_H
#define LIBRARY_ARCHIVE_EXT_AREARCHIVE_H

#include "aremain.h"
#include "areerror.h"
#include "areentry.h"

G_BEGIN_DECLS

typedef struct archive AREArchive;

G_DEFINE_AUTOPTR_CLEANUP_FUNC(AREArchive, archive_free)

void are_archive_set_error(AREArchive *self, GError **error);

gint are_archive_read_support_filter_gzip(AREArchive *self, GError **error);
gint are_archive_read_support_format_all(AREArchive *self, GError **error);
gint are_archive_read_support_format_tar(AREArchive *self, GError **error);
gint are_archive_read_open_filename(AREArchive *self, gchar *filename, gsize n, GError **error);
gint are_archive_read_open_memory(AREArchive *self, gpointer data, gsize n, GError **error);
gint are_archive_read_next_header(AREArchive *self, AREEntry **entry, GError **error);
gssize are_archive_read_data(AREArchive *self, gpointer data, gsize n, GError **error);
gint are_archive_read_extract(AREArchive *self, AREEntry *entry, gint flags, GError **error);

G_END_DECLS

#endif //LIBRARY_ARCHIVE_EXT_AREARCHIVE_H
