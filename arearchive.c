//
// Created by root on 06.03.2021.
//

#include "arearchive.h"

void are_archive_set_error(AREArchive *self, GError **error) {
    gint code = archive_errno(self);
    gchar *message = (gchar *)archive_error_string(self);
    g_set_error_literal(error, ARE_ERROR, code, message);
}

gint are_archive_read_support_filter_gzip(AREArchive *self, GError **error) {
    gint ret = archive_read_support_filter_gzip(self);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gint are_archive_read_support_format_all(AREArchive *self, GError **error) {
    gint ret = archive_read_support_format_all(self);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gint are_archive_read_support_format_tar(AREArchive *self, GError **error) {
    gint ret = archive_read_support_format_tar(self);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gint are_archive_read_open_filename(AREArchive *self, gchar *filename, gsize n, GError **error) {
    gint ret = archive_read_open_filename(self, filename, n);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gint are_archive_read_open_memory(AREArchive *self, gpointer data, gsize n, GError **error) {
    gint ret = archive_read_open_memory(self, data, n);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gint are_archive_read_next_header(AREArchive *self, AREEntry **entry, GError **error) {
    gint ret = archive_read_next_header(self, entry);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gssize are_archive_read_data(AREArchive *self, gpointer data, gsize n, GError **error) {
    gssize ret = archive_read_data(self, data, n);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}

gint are_archive_read_extract(AREArchive *self, AREEntry *entry, gint flags, GError **error) {
    gint ret = archive_read_extract(self, entry, flags);

    if (ret < ARCHIVE_OK) {
        are_archive_set_error(self, error);
    }

    return ret;
}
