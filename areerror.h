//
// Created by root on 06.03.2021.
//

#ifndef LIBRARY_ARCHIVE_EXT_AREERROR_H
#define LIBRARY_ARCHIVE_EXT_AREERROR_H

#include "aremain.h"

G_BEGIN_DECLS

#define ARE_ERROR are_error_quark()

GQuark are_error_quark(void);

G_END_DECLS

#endif //LIBRARY_ARCHIVE_EXT_AREERROR_H
