//
// Created by root on 19.10.2021.
//

#ifndef LIBRARY_ARCHIVE_EXT_AREINIT_H
#define LIBRARY_ARCHIVE_EXT_AREINIT_H

#include "aremain.h"

G_BEGIN_DECLS

void are_init(void);

G_END_DECLS

#endif //LIBRARY_ARCHIVE_EXT_AREINIT_H
